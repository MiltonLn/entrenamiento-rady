"""entrenamiento URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
# from django.contrib import admin

urlpatterns = patterns('',
    url(r'^', include('reportes.urls')),
    url(r'^profesores/', include('profesores.urls')),
    url(r'^estudiantes/', include('estudiantes.urls')),
    url(r'^calificaciones/', include('calificaciones.urls')),
    url(r'^login$', 'django.contrib.auth.views.login', {'template_name':'login.html'}, name='login'),
    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page': 'inicio'}, name='logout'),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

