from django.conf.urls import include, url, patterns
from django.conf import settings
from django.shortcuts import redirect
from django.conf.urls.static import static


urlpatterns = patterns(
    'reportes.views',
    url(r'^$', 'mostrar_inicio', name='index'),
    url(r'^inicio$', 'mostrar_inicio', name = 'inicio'),
    url(r'^reportes/barras', 'mostrar_grafica_barras', name='grafica_barras'),
    url(r'^reportes/actualizar', 'actualizar_reporte', name='actualizar_reporte'),
    url(r'^reportes/nota/', 'reporte_nota', name='reporte_nota')

)