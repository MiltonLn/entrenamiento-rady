/**
 * Created by Diego Monsalve on 18/02/16.
 */

form = "#profesor-form";
fields = {
    nombre: {
        validators: {
            notEmpty: {
                message: "El nombre del profesor no puede ser vacío"
            }
        }
    },
    tipo_documento: {
        validators: {
            notEmpty: {
                message: "El tipo de documento no puede ser vacío"
            }
        }
    },
    numero_documento: {
        validators: {
            notEmpty: {
                message: "El número de documento no puede ser vacío"
            },
            digits: {
                message: "El número de documento no es válido"
            }
        }
    },
    fecha_nacimiento: {
        validators: {
            notEmpty: {
                message: "La fecha de nacimiento no puede ser vacío"
            },
            date: {
                format: "YYYY-MM-DD",
                message: "La fecha de nacimiento no es válida"
            }
        }
    },
    correo: {
        validators: {
            notEmpty: {
                message: "El correo electrónico no puede ser vacío"
            },
            emailAddress: {
                message: "La dirección de correo no es válida"
            }
        }
    }

};

$("#id_fecha_nacimiento").on('change',function(e){
   $(form).bootstrapValidator('revalidateField', 'fecha_nacimiento');
});
$.getScript(base+"js/validaciones/validations-base.js");