from django import forms
from profesores.models import Profesor
from entrenamiento.utilities import MyDateWidget
import datetime


class ProfesorForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProfesorForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget.attrs['type']='password';
        if self.instance.pk != None:
            self.fields['tipo_documento'].widget.attrs['readonly'] = 1
            self.fields['numero_documento'].widget.attrs['readonly'] = 1
            self.fields['username'].widget.attrs['readonly'] = 1

    class Meta:
        model = Profesor
        exclude = ('is_active', 'last_login', 'groups', 'user_permissions', 'is_staff', 'date_joined', 'is_superuser',)
        widgets = {'fecha_nacimiento':MyDateWidget()}

    def clean(self):
        cleaned_data = super(ProfesorForm, self).clean()
        if not self._errors:
            try:
                fecha_nacimiento = cleaned_data.get('fecha_nacimiento')
            except Exception:
                fecha_nacimiento = None

            fecha_actual = datetime.date.today()
            if fecha_nacimiento:
                if fecha_nacimiento >= fecha_actual:
                    mensaje = 'La fecha de nacimiento no es válida. Debe ser antes de la fecha actual'
                    self.add_error('fecha_nacimiento', mensaje)
                else:
                    return cleaned_data
            else:
                return cleaned_data

        return cleaned_data