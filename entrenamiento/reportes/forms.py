from django import forms


class FormularioReporteBarras(forms.Form):

    def __init__(self, *args, **kwargs):
        super(FormularioReporteBarras, self).__init__(*args, **kwargs)
        self.fields['anio'].widget.attrs.update({'class': 'many'})

    anios = (
        (6, '6'),
        (7, '7'),
        (8, '8'),
        (9, '9'),
        (10, '10'),
        (11, '11'),
    )

    anio = forms.MultipleChoiceField(choices=anios, label="Año", required=False)

class FormularioFiltros(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FormularioFiltros, self).__init__(*args, **kwargs)
        self.fields['anio'].widget.attrs.update({'class': 'many'})
        self.fields['materia'].widget.attrs.update({'class': 'many'})

    materias = (
        ('Español', 'Español'),
        ('Matematicas', 'Matemáticas'),
        ('Biologia', 'Biología'),
        ('Sociales', 'Sociales'),
        ('Filosofia', 'Filosofía'),
        ('Informatica', 'Informática'),
        ('Deporte', 'Deporte'),
    )
    anios = (
        (6, '6'),
        (7, '7'),
        (8, '8'),
        (9, '9'),
        (10, '10'),
        (11, '11'),
    )

    materia = forms.MultipleChoiceField(choices=materias, label='Materias', required=False)
    anio = forms.MultipleChoiceField(choices=anios, label="Año", required=False)