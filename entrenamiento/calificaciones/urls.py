from django.conf.urls import include, url, patterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('calificaciones.views',
    url(r'^registrar/', 'crear', name='calificacion_crear'),
    url(r'^listar/', 'listar', name='calificaciones_listar'),
    url(r'^editar/(\d+)$', 'editar', name='calificacion_editar'),
    url(r'^eliminar/(\d+)$', 'eliminar', name='calificacion_eliminar'),
)

