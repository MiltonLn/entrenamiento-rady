from django.conf.urls import include, url, patterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('estudiantes.views',
    url(r'^crear/', 'crear', name='estudiante_crear'),
    url(r'^listar/', 'listar', name='estudiantes_listar'),
    url(r'^editar/(\d+)$', 'editar', name='estudiante_editar'),
    url(r'^cambiar/(\d+)$', 'cambiar_estado', name='estudiante_estado'),
)