from django.db import models
from django.contrib.auth.models import User


class Profesor(User):

    tipos_documento = (
        ('CC', 'Cédula de Ciudadanía'),
        ('TI', 'Tarjeta de Identidad'),
        ('PAS', 'Pasaporte'),
        ('CE', 'Cédula de Extranjería'),
    )

    tipo_documento = models.CharField(max_length=30, choices=tipos_documento, verbose_name="Tipo de Documento")
    numero_documento = models.CharField(max_length=20, verbose_name="Número de Documento", unique=True)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento")
    foto = models.ImageField(upload_to = 'profesores', verbose_name="Foto", blank=True, default="profesores/")

    def __str__(self):
        return self.first_name
