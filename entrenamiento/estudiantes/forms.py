import datetime
from django import forms
from estudiantes.models import Estudiante
from entrenamiento.utilities import MyDateWidget

TIPO_DOCUMENTO=(
        ('TI', 'Tarjeta de Identidad'),
        ('CC', 'Cedula de Ciudadania'),
        ('PS', 'Pasaporte'),

)

class FormularioCrear(forms.ModelForm):


    def __init__(self, *args, **kwargs):
        super(FormularioCrear, self).__init__(*args, **kwargs)

    class Meta:
        model = Estudiante
        exclude = ('estado',)
        widgets = {
            'fecha_nacimiento': MyDateWidget(),
        }

    def clean(self):
        cleaned_data = super(FormularioCrear, self).clean()
        if not self._errors:
            try:
                fecha_nacimiento = cleaned_data.get('fecha_nacimiento')
            except Exception:
                fecha_nacimiento = None

            fecha_actual = datetime.date.today()
            if fecha_nacimiento:
                if fecha_nacimiento > fecha_actual:
                    mensaje = 'Esta no es una fecha de nacimiento valida. Debe ser antes de la fecha actual'
                    self.add_error('fecha_nacimiento', mensaje)
                else:
                    return cleaned_data
            else:
                return cleaned_data

        return cleaned_data


class FormularioEditar(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FormularioEditar, self).__init__(*args, **kwargs)
        self.fields['tipo_documento'].widget.attrs['readonly'] = 1
        self.fields['identificacion'].widget.attrs['readonly'] = 1

    class Meta:
        model = Estudiante
        exclude = ('estado',)
        widgets = {
            'fecha_nacimiento': MyDateWidget(),
        }
    def clean(self):
        cleaned_data = super(FormularioEditar, self).clean()
        if not self._errors:
            try:
                fecha_nacimiento = cleaned_data.get('fecha_nacimiento')
            except Exception:
                fecha_nacimiento = None

            fecha_actual = datetime.date.today()
            if fecha_nacimiento:
                if fecha_nacimiento > fecha_actual:
                    mensaje = 'Esta no es una fecha de nacimiento valida. Debe ser antes de la fecha actual'
                    self.add_error('fecha_nacimiento', mensaje)
                else:
                    return cleaned_data
            else:
                return cleaned_data

        return cleaned_data