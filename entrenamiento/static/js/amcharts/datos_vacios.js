function cantidadesCero(datos){
    var count = 0;
    var ceros = false;
    for (i=0; i<datos.length; i++){
        if (datos[i].cantidad == 0){
            count++;
        }
    }
    if (count == datos.length){
        ceros = true;
    }
    return ceros;
}

AmCharts.checkEmptyData = function (chart) {

    if ( 0 == chart.dataProvider.length || cantidadesCero(chart.dataProvider || chart.dataProvider == undefined)) {
        // add label
        try{
            chart.addLabel(0, '50%', 'No hay datos para el reporte especificado', 'center', 19, "#84c4e2");
        }catch(e){

        }

    }else{
        chart.allLabels = [];
    }
    chart.validateNow();

}