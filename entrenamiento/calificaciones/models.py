from django.db import models
from profesores.models import Profesor
from estudiantes.models import Estudiante


class Calificacion(models.Model):

    etiquetas = (
        ('E', 'Excelente'),
        ('S', 'Sobresaliente'),
        ('A', 'Aceptable'),
        ('I', 'Insuficiente'),
        ('D', 'Deficiente'),
    )

    materias = (
        ('Español', 'Español'),
        ('Matematicas', 'Matemáticas'),
        ('Biologia', 'Biología'),
        ('Sociales', 'Sociales'),
        ('Filosofia', 'Filosofía'),
        ('Informatica', 'Informática'),
        ('Deporte', 'Deporte'),
    )

    profesor = models.ForeignKey(Profesor)
    estudiante = models.ForeignKey(Estudiante)
    nota = models.DecimalField(max_digits=2, decimal_places=1, verbose_name="Calificación")
    etiqueta = models.CharField(max_length=15, choices=etiquetas)
    anio = models.IntegerField(verbose_name="Año del estudiante")
    materia = models.CharField(max_length=30, choices=materias, verbose_name="Materia de calificación")
    descripcion = models.TextField(max_length=500, verbose_name="Observaciones", null=True)

