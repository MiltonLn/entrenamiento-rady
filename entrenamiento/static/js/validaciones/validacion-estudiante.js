form = "#estudiante-form";

fields = {
    nombre: {
        validators: {
            notEmpty: {
                message: "El nombre del estudiante no puede ser vacío"
            }
        }
    },
    apellidos: {
        validators: {
            notEmpty: {
                message: "El apellido del estudiante no puede ser vacío"
            }
        }
    },
    identificacion: {
        validators: {
            notEmpty: {
                message: "El documento del estudiante no puede ser vacío"
            },
            digits:{
                message: "El documento debe ser un numero"
            }
        }
    },
    fecha_nacimiento: {
        validators: {
            notEmpty: {
                message: "Elija una fecha"
            },
            date: {
                format: "YYYY-MM-DD",
                message: "No es una fecha valida"
            }
        }
    },
    tipo_documento: {
        validators: {
            notEmpty: {
                message: "Elija un tipo de documento"
            }
        }
    }
};

$.getScript(base+"js/validaciones/validations-base.js");

//Revalidar campos al ser actualizados
    $("#id_fecha_nacimiento").on('change',function(e){
        $(form).bootstrapValidator('revalidateField', 'fecha_nacimiento');
    });