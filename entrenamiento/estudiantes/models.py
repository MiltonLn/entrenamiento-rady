from django.db import models

# Create your models here.
class Estudiante(models.Model):

    TIPO_DOCUMENTO=(
        ('TI', 'Tarjeta de Identidad'),
        ('CC', 'Cédula de Ciudadania'),
        ('PS', 'Pasaporte'),

    )
    ESTADO =(
        (1, 'ACTIVO'),
        (0, 'INACTIVO'),
    )

    nombre = models.CharField(max_length=100, verbose_name="Nombre")
    apellidos = models.CharField(max_length=100, verbose_name="Apellidos")
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento")
    tipo_documento= models.CharField(max_length=20, choices=TIPO_DOCUMENTO, verbose_name="Tipo de identificación")
    identificacion= models.CharField(max_length=20, verbose_name="Número de identificación", unique=True)
    foto= models.ImageField(upload_to='fotos_estudiantes', blank=True, default='fotos_estudiantes/default.jpg')
    estado = models.IntegerField(choices=ESTADO, default=1)

    def __str__(self):
        return self.nombre + " "+self.apellidos
