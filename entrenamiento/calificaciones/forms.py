from django import forms
from calificaciones.models import Calificacion

class FormularioCalificacion(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FormularioCalificacion, self).__init__(*args, **kwargs)
        self.fields['nota'].widget.attrs.update({'max':5})
        self.fields['nota'].widget.attrs.update({'min':0})
        if self.instance.pk != None:
            self.fields['estudiante'].widget.attrs['readonly'] = 1
            self.fields['profesor'].widget.attrs['readonly'] = 1
            self.fields['materia'].widget.attrs['readonly'] = 1
            self.fields['anio'].widget.attrs['readonly'] = 1


    class Meta:
        model = Calificacion
        exclude= ('etiqueta',)

    def clean(self):
        cleaned_data = super(FormularioCalificacion, self).clean()

        if not self._errors:
            try:
                nota = cleaned_data.get('nota')
            except Exception:
                nota = None

            if nota:
                if nota > 5:
                    mensaje = "La nota no puede ser mayor a 5"
                    self.add_error('nota', mensaje)
                else:
                    return cleaned_data
            else:
                return cleaned_data
        return cleaned_data