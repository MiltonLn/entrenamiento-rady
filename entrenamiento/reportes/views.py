from django.shortcuts import render
import json
from django.http import JsonResponse
from estudiantes.models import Estudiante
from calificaciones.models import Calificacion
from profesores.models import Profesor
from .forms import FormularioReporteBarras, FormularioFiltros
from django.db.models import Count


def mostrar_inicio(request):
    estudiantes = Estudiante.objects.all().count()
    profesores = Profesor.objects.all().count()
    return render(request, 'inicio.html', {'estudiantes':estudiantes, 'profesores':profesores})

def mostrar_grafica_barras(request):
    formulario = FormularioReporteBarras(request.POST)
    lista = Calificacion.objects.values('materia').filter(nota__gte = 3).annotate(cantidad=Count('materia'))
    return render(request, 'reporte_barras.html', {'aprobados':lista, 'formulario':formulario})

def actualizar_reporte(request):
    anios = request.GET.getlist('anios[]')
    print(anios)
    if not anios or anios[0] == '':
        data = Calificacion.objects.values('materia').filter(nota__gte = 3).annotate(cantidad=Count('materia'))
    else:
        data = Calificacion.objects.values('materia').filter(anio__in=anios).filter(nota__gte = 3).annotate(cantidad=Count('materia'))
    lista_final = {}
    lista_final['lista']=list(data)
    return JsonResponse(lista_final)

def reporte_nota(request):

    forms = FormularioFiltros()
    lista_estudiantes = {}
    materias = []
    anios = []
    if request.is_ajax():
        try:
            materias = request.POST.getlist('materias[]')
            anios = request.POST.getlist('anios[]')

            if materias[0]!='':
                if   anios[0]!='':
                    lista_estudiantes = generar_datos(materias, anios)
                    return JsonResponse(lista_estudiantes)
                else:
                    anios=None
                    lista_estudiantes = generar_datos(materias, anios)
                    return JsonResponse(lista_estudiantes)
            elif anios[0]!='':
                materias = None
                lista_estudiantes = generar_datos(materias, anios)
                return JsonResponse(lista_estudiantes)
            else:
                materias = None
                anios = None
                lista_estudiantes = generar_datos(materias, anios)
                return JsonResponse(lista_estudiantes)


        except Exception as e:
            print (e)
    else:
        lista_estudiantes = json.dumps(generar_datos(materias, anios))

    return render(request, 'reporte_estudiante.html', {'lista': lista_estudiantes, 'forms': forms})

def generar_datos(materias, anios):

    if materias and anios:
        lista = Calificacion.objects.filter(materia__in=materias, anio__in=anios).distinct('estudiante', 'materia', 'etiqueta')
    elif materias:

        lista = Calificacion.objects.filter(materia__in=materias).distinct('estudiante', 'materia', 'etiqueta')
    elif anios:
        lista = Calificacion.objects.filter(anio__in=anios).distinct('estudiante', 'materia', 'etiqueta')
    else:

        lista = Calificacion.objects.distinct('estudiante', 'materia', 'etiqueta')

    lista_estudiantes = []

    notas = {}
    notas['cantidad'] = lista.filter(etiqueta='D').count()
    notas['etiqueta']='Deficiente'
    lista_estudiantes.append(notas)

    notas = {}
    notas['cantidad'] = lista.filter(etiqueta='I').count()
    notas['etiqueta']='Insuficiente'
    lista_estudiantes.append(notas)

    notas = {}
    notas['cantidad'] = lista.filter(etiqueta='A').count()
    notas['etiqueta']='Aceptable'
    lista_estudiantes.append(notas)

    notas = {}
    notas['cantidad'] = lista.filter(etiqueta='S').count()
    notas['etiqueta']='Sopbresaliente'
    lista_estudiantes.append(notas)

    notas = {}
    notas['cantidad'] = lista.filter(etiqueta='E').count()
    notas['etiqueta']='Excelente'
    lista_estudiantes.append(notas)

    lista_final = {}
    lista_final['lista_estudiantes']=lista_estudiantes

    return lista_final
