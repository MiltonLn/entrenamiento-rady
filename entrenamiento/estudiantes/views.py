import datetime
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from estudiantes.forms import FormularioCrear, FormularioEditar
from estudiantes.models import Estudiante

@login_required
def crear(request):
    if request.method == 'POST':
        form = FormularioCrear(request.POST, request.FILES) #Se agrega el FILES para lograr carghar los archivos
        if form.is_valid():
            form.save()
            messages.success(request, "Estudiante guardado correctamente.")
            return redirect('estudiantes_listar')
    else:
        form= FormularioCrear()

    return render(request, 'crear_estudiante.html', {'form': form})

def listar(request):
    estudiantes = Estudiante.objects.all()

    return render(request, 'listar_estudiantes.html', {'lista': estudiantes})

@login_required
def editar(request, estudiante_id):
    try:
        estudiante = Estudiante.objects.get(pk=estudiante_id)
    except:
        estudiante = None
        return redirect('estudiantes_listar')
    form = FormularioEditar(instance=estudiante)
    if request.method == 'POST':
        form = FormularioEditar(request.POST, request.FILES, instance=estudiante)
        if form.is_valid():
            form.save()
            messages.success(request, "Datos de %s guardados correctamente.")%(estudiante.nombre)
            return redirect('estudiantes_listar')

    else:
        form = FormularioEditar(instance=estudiante)


    return render(request, 'editar_estudiante.html', {'form':form})

@login_required
def cambiar_estado(request, estudiante_id):
    try:
        estudiante = Estudiante.objects.get(pk=estudiante_id)
        if estudiante.estado == 0:
            estudiante.estado = 1
            estudiante.save()
            messages.success(request, "Se ha cambiado el estado del estudiante %s")%(estudiante.nombre)
        else:
            estudiante.estado = 0
            estudiante.save()
            messages.success(request, "Se ha cambiado el estado del estudiante %s")%(estudiante.nombre)
    except:
        estudiante = None
        return redirect('estudiantes_listar')

    return redirect('estudiantes_listar')
