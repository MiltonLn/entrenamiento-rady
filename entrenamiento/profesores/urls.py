from django.conf.urls import patterns, url

urlpatterns = patterns(
    'profesores.views',
    url(r'^registrar', 'registrar_profesor', name='registrar_profesor'),
    url(r'^listar', 'listar_profesores', name='listar_profesores'),
    url(r'^modificar/(\d+)', 'modificar_profesor', name='modificar_profesor'),
    url(r'^eliminar/(\d+)', 'eliminar_profesor', name='eliminar_profesor'),
)