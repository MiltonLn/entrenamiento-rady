form = "#calificacion-form";
fields = {
    profesor: {
        validators: {
            notEmpty: {
                message: "El profesor no puede ser vacío."
            }
        }
    },
    estudiante: {
        validators: {
            notEmpty: {
                message: "El estudiante no puede ser vacío"
            }
        }
    },
    nota: {
        validators: {
            notEmpty: {
                message: "La calificacion no puede ser vacía"
            },
            numeric:{
                message: "La nota debe ser un número. El separador decimal es '.'",
                decimalSeparator: '.'
            }
        }
    },
    anio: {
        validators: {
            notEmpty: {
                message: "El año del estudiante no puede estar vacío"
            },
            digits: {
                message: "No es una numero valido"
            }
        }
    },
    materia: {
        validators: {
            notEmpty: {
                message: "La materia no puede ser vacía"
            }
        }
    }
};

$.getScript(base+"js/validaciones/validations-base.js");