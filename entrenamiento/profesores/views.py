from django.shortcuts import render, redirect
from .forms import ProfesorForm
from .models import Profesor
from django.contrib.auth.decorators import login_required
from django.contrib import messages


def registrar_profesor(request):
    if request.method == 'POST':
        form = ProfesorForm(request.POST, request.FILES)
        if form.is_valid():
            formulario = form.save(commit=False)
            formulario.set_password(formulario.password)
            formulario.save()
            messages.success(request, ("Profesor(a) %s registrado correctamente.")%(formulario.first_name))
            return redirect('listar_profesores')
        else:
            return render(request, 'registrar_profesor.html', {'form':form})
    else:
        form = ProfesorForm()
        return render(request, 'registrar_profesor.html', {'form':form})


def listar_profesores(request):
    lista_profesores = Profesor.objects.all()
    return render(request, 'listar_profesores.html', {'profesores':lista_profesores})


@login_required
def modificar_profesor(request, id_profesor):
    try:
        profesor = Profesor.objects.get(id=id_profesor)
    except:
        return redirect('listar_profesores')

    form = ProfesorForm(instance=profesor)

    if request.method=='POST':
        form = ProfesorForm(request.POST, request.FILES, instance=profesor)
        if form.has_changed():
            if form.is_valid():
                formulario = form.save(commit=False)
                formulario.set_password(formulario.password)
                form.save()
                messages.success(request, ("Datos de %s guardados correctamente.")%(profesor.first_name))
                return redirect('listar_profesores')
        else:
            return redirect('listar_profesores')
    return render(request, 'registrar_profesor.html', {'form':form, 'edicion':True})


@login_required
def eliminar_profesor(request, id_profesor):
    try:
        profesor = Profesor.objects.get(id=id_profesor)
        if profesor.is_active:
            profesor.is_active = False
            messages.success(request, ("La cuenta de %s ha sido desactivada.")%(profesor.first_name))
        else:
            profesor.is_active = True;
            messages.success(request, ("La cuenta de %s ha sido activada.")%(profesor.first_name))
        profesor.save()

        return redirect('listar_profesores')
    except Exception:
        return redirect('listar_profesores')

