from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from calificaciones.forms import FormularioCalificacion
from calificaciones.models import Calificacion
from django.contrib import messages

@login_required
def crear(request):
    if request.method == 'POST':
        form = FormularioCalificacion(request.POST)
        if form.is_valid():
            formulario = form.save(commit=False)
            nota = formulario.nota
            if nota < 2:
                formulario.etiqueta = 'D'
            elif nota < 3:
                formulario.etiqueta = 'I'
            elif nota < 4:
                formulario.etiqueta = 'A'
            elif nota < 5:
                formulario.etiqueta = 'S'
            else:
                formulario.etiqueta = 'E'
            messages.success(request, "Calificación guardada correctamente.")
            form.save()
            return redirect('calificaciones_listar')
    else:
        form=FormularioCalificacion()
    return render(request, 'registrar_calificacion.html', {'form': form, 'edicion': False})


@login_required
def editar(request, calificacion_id):
    try:
        calificacion = Calificacion.objects.get(pk=calificacion_id)
    except:
        calificacion = None
        return redirect('calificaciones_listar')
    form = FormularioCalificacion(instance=calificacion)
    if request.method == 'POST':
        form = FormularioCalificacion(request.POST, instance=calificacion)
        if form.is_valid():
            formulario = form.save(commit=False)
            nota = formulario.nota
            if nota < 2:
                formulario.etiqueta = 'D'
            elif nota < 3:
                formulario.etiqueta = 'I'
            elif nota < 4:
                formulario.etiqueta = 'A'
            elif nota < 5:
                formulario.etiqueta = 'S'
            else:
                formulario.etiqueta = 'E'
            messages.success(request, "Datos de la calificación guardados correctamente.")
            form.save()
            return redirect('calificaciones_listar')
    else:
        form = FormularioCalificacion(instance=calificacion)
    return render(request, 'registrar_calificacion.html', {'form':form, 'edicion': True})


def listar(request):
    calificaciones = Calificacion.objects.all()
    return render(request, 'listar_calificaciones.html', {'calificaciones': calificaciones})



@login_required
def eliminar(request, calificacion_id):
    try:
        calificacion = Calificacion.objects.get(pk=calificacion_id)
        calificacion.delete()
        messages.success(request, "La calificación ha sido eliminada.")
    except:
        calificacion = None
        return redirect('calificaciones_listar')
    return redirect('calificaciones_listar')